package main

import (
	"crypto/rand"
	"encoding/gob"

	"bitbucket.org/rmanolis/cblib"
	"github.com/gopherjs/gopherjs/js"
)

type Key struct {
	PrivateKey string
	PublicKey  string
}

func main() {
	gob.Register(cblib.Signature{})
	exports := js.Module.Get("exports")
	exports.Set("GenerateKeys", GenerateKeys)
	exports.Set("Verify", Verify)
	exports.Set("VerifyFile", VerifyFile)
	exports.Set("Sign", Sign)
	exports.Set("SignFile", SignFile)
	exports.Set("Encrypt", Encrypt)
	exports.Set("EncryptFile", EncryptFile)
	exports.Set("Decrypt", Decrypt)
	exports.Set("DecryptFile", DecryptFile)

}

func GenerateKeys(curve string) (*Key, error) {
	if curve == "" {
		curve = "P256"
	}
	key, err := cblib.GenerateKeys(curve, rand.Reader)
	if err != nil {
		return nil, err
	}
	skey := Key{PublicKey: string(key.PublicKey),
		PrivateKey: string(key.PrivateKey)}
	return &skey, nil
}

func Verify(pub string, str string, sigb string) (bool, error) {
	return cblib.Verify([]byte(pub), []byte(str), []byte(sigb))
}

func VerifyFile(pub string, file []byte, sigb string) (bool, error) {
	return cblib.Verify([]byte(pub), file, []byte(sigb))
}

func Sign(prv, text string) (string, error) {
	b, err := cblib.Sign([]byte(text), []byte(prv), rand.Reader)
	return string(b), err
}

func SignFile(prv string, file []byte) (string, error) {
	b, err := cblib.Sign(file, []byte(prv), rand.Reader)
	return string(b), err
}

func Encrypt(priv, pub, text string) (string, error) {
	b, err := cblib.Encrypt([]byte(priv), []byte(pub), []byte(text))
	return string(b), err
}

func EncryptFile(priv, pub string, file []byte) ([]byte, error) {
	b, err := cblib.Encrypt([]byte(priv), []byte(pub), file)
	return b, err
}

func Decrypt(priv, pub, text string) (string, error) {
	b, err := cblib.Decrypt([]byte(priv), []byte(pub), []byte(text))
	return string(b), err
}

func DecryptFile(priv, pub string, file []byte) ([]byte, error) {
	b, err := cblib.Decrypt([]byte(priv), []byte(pub), file)
	return b, err
}
