var cblib = require('../index.js');
var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai


describe('cblibjs', function() {
  it('Verify() should return true', function() {
   let [key, er1] = cblib.GenerateKeys('P256')
   let text = "lalalalalallalallalalalalalallalalalallalalala"
   let [signature, er2] = cblib.Sign(key.PrivateKey, text)
   let [b, er3] = cblib.Verify(key.PublicKey, text, signature)
   expect(b).to.equal(true);
  });

  it('Verify() should return false', function() {
    let [key, er1] = cblib.GenerateKeys('P256')
    let text = "lalalalalallalallalalalalalallalalalallalalala"
    let text1 = "lalalalalallalallalalalalalallalalalallalalal"
    let [signature, er2] = cblib.Sign(key.PrivateKey, text)
    let [b, er3] = cblib.Verify(key.PublicKey, text1, signature)
    expect(b).to.equal(false);
  });

  it('the input of Encrypt() should be equal to the output of the Decrypt()',function(){
    let [keyA, er1] = cblib.GenerateKeys('P256')
    let [keyB, er2] = cblib.GenerateKeys('P256')
    let text = "lalalalalallalallalalalalalallalalalallalalala"
    let [encrypted, er3] = cblib.Encrypt(keyA.PrivateKey, keyB.PublicKey, text)
    let [decrypted, er4] = cblib.Decrypt(keyB.PrivateKey, keyA.PublicKey, encrypted)
    expect(decrypted === text).to.equal(true);
   
  });

});