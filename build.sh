#!/bin/bash
go get -u bitbucket.org/rmanolis/cblib
gopherjs build cbdsa_lib/cbdsa_lib.go -m -o tmp-index.js
sed 's/$global.require=require;//g' tmp-index.js > index.js
rm tmp-index.js
mv tmp-index.js.map index.js.map
